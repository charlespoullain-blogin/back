FROM node:11-alpine

WORKDIR /app

COPY . /app

RUN npm i

EXPOSE 3000

CMD [ "npm", "run", "start" ]